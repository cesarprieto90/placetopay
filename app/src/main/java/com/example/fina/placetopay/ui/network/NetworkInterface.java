package com.example.fina.placetopay.ui.network;


import com.example.fina.placetopay.ui.model.Data_Additional;
import com.example.fina.placetopay.ui.model.Data_Object;
import com.example.fina.placetopay.ui.model.Data_Response_Object;
import com.example.fina.placetopay.ui.model.Data_Search;
import com.example.fina.placetopay.ui.model.Data_Search_Response;


import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.POST;


/**
 * Created by anujgupta on 26/12/17.
 */

public interface NetworkInterface {

    @POST("gateway/process")
    Observable<Data_Response_Object> getPayment(@Body Data_Object dataObject);


    @POST("gateway/search")
    Observable<Data_Search_Response> getPayment(@Body Data_Search dataSearch);

}
