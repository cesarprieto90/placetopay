package com.example.fina.placetopay.ui.model;

public class Data_Search {

    private Data_Auth auth= new Data_Auth();
    private String reference="";
    private Data_Amount amount= new Data_Amount();

    public Data_Search(Data_Auth auth, String reference, Data_Amount amount) {
        this.auth = auth;
        this.reference = reference;
        this.amount = amount;
    }

    public Data_Search() {
    }

    public Data_Auth getAuth() {
        return auth;
    }

    public void setAuth(Data_Auth auth) {
        this.auth = auth;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public Data_Amount getAmount() {
        return amount;
    }

    public void setAmount(Data_Amount amount) {
        this.amount = amount;
    }
}
