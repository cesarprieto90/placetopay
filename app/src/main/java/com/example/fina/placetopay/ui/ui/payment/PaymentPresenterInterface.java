package com.example.fina.placetopay.ui.ui.payment;

import com.example.fina.placetopay.ui.model.Data_Object;
import com.example.fina.placetopay.ui.model.Data_Usuario;

/**
 * Created by anujgupta on 26/12/17.
 */

public interface PaymentPresenterInterface {

    void getPayment(Data_Object dataObject);

}
