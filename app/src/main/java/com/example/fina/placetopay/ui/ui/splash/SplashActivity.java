package com.example.fina.placetopay.ui.ui.splash;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import androidx.appcompat.app.AppCompatActivity;

import com.example.fina.placetopay.R;
import com.example.fina.placetopay.ui.ui.login.LoginActivity;


public class SplashActivity extends AppCompatActivity {

    private int tiempoSplash=1500;
    private Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        handler= new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mainActivity();
            }
        },tiempoSplash);
    }

    private void mainActivity() {
        Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
        startActivity(intent);
    }
}
