package com.example.fina.placetopay.ui.ui.Adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.example.fina.placetopay.R;
import com.example.fina.placetopay.ui.model.Data_Response_Object;
import com.example.fina.placetopay.ui.ui.payment.PaymentActivity;

import java.util.ArrayList;

import io.realm.RealmResults;

public class TransaccionAdapter extends RecyclerView.Adapter<TransaccionAdapter.OrdenHolder> {

    private RealmResults<Data_Response_Object> dataResponse;
    private Context context;

    public TransaccionAdapter(RealmResults<Data_Response_Object> dataProductos, Context context) {
        this.dataResponse = dataProductos;
        this.context = context;
    }

    @Override
    public OrdenHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.item_transaccion, parent, false);
        OrdenHolder mh = new OrdenHolder(v);

        return mh;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onBindViewHolder(OrdenHolder holder, int position) {
        try {
            holder.lblTotal.setText("Total: "+dataResponse.get(position).getAmount().getTotal());
            holder.lblReferencia.setText("Referencia: "+ dataResponse.get(position).getReference());
            holder.lblEstado.setText(dataResponse.get(position).getStatus().getStatus());
            holder.lblMensaje.setText(dataResponse.get(position).getStatus().getMessage());
            holder.lblFecha.setText("Fecha: "+dataResponse.get(position).getStatus().getDate());
            holder.setItemClickListener(new ItemClickListener() {
                @Override
                public void onClick(View view, int position, boolean isLongClick) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return dataResponse.size();
    }

    public class OrdenHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView lblFecha, lblEstado, lblReferencia, lblTotal,lblMensaje;

        private ItemClickListener itemClickListener;

        public OrdenHolder(View v) {
            super(v);
            lblFecha = v.findViewById(R.id.lblFecha);
            lblMensaje = v.findViewById(R.id.lblMensaje);
            lblEstado = v.findViewById(R.id.lblEstado);
            lblReferencia = v.findViewById(R.id.lblReferencia);
            lblTotal = v.findViewById(R.id.lblTotal);
            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            itemClickListener.onClick(view, getAdapterPosition(), false);
        }

        public void setItemClickListener(ItemClickListener itemClickListener) {
            this.itemClickListener = itemClickListener;
        }
    }
}
