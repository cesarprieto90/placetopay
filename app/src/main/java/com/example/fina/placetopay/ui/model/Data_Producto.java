package com.example.fina.placetopay.ui.model;

public class Data_Producto {
    String name;
    String version;
    int precio;
    int id_;
    int image;

    public Data_Producto(String name, String version, int precio, int id_, int image) {
        this.name = name;
        this.version = version;
        this.precio = precio;
        this.id_ = id_;
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public int getPrecio() {
        return precio;
    }

    public void setPrecio(int precio) {
        this.precio = precio;
    }

    public int getId_() {
        return id_;
    }

    public void setId_(int id_) {
        this.id_ = id_;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }
}
