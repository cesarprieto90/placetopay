package com.example.fina.placetopay.ui.ui.buy.ui.HisPayment;

import com.example.fina.placetopay.ui.model.Data_Object;
import com.example.fina.placetopay.ui.model.Data_Search;

/**
 * Created by anujgupta on 26/12/17.
 */

public interface HisPaymentPresenterInterface {

    void gethisPayment(Data_Search dataSearch);
}
