package com.example.fina.placetopay.ui.model;

import com.example.fina.placetopay.ui.Utils.Constants;

import io.realm.RealmList;

public class Data_Object {
    private Data_Auth auth=new Data_Auth();
    private String locale= Constants.Locale;
    private Data_Payment payment=new Data_Payment();
    private String ipAddress="127.0.0.1";
    private String userAgent="Mozilla/5.0 USER_AGENT HERE";
    private Data_Additional additional=new Data_Additional();
    private Data_Instrument instrument=new Data_Instrument();
    private Data_Payer payer;
    private Data_Payer buyer;

    public Data_Auth getAuth() {
        return auth;
    }

    public void setAuth(Data_Auth auth) {
        this.auth = auth;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public Data_Payment getPayment() {
        return payment;
    }

    public void setPayment(Data_Payment payment) {
        this.payment = payment;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getUserAgent() {
        return userAgent;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    public Data_Additional getAdditional() {
        return additional;
    }

    public void setAdditional(Data_Additional additional) {
        this.additional = additional;
    }

    public Data_Instrument getInstrument() {
        return instrument;
    }

    public void setInstrument(Data_Instrument instrument) {
        this.instrument = instrument;
    }

    public Data_Payer getPayer() {
        return payer;
    }

    public void setPayer(Data_Payer payer) {
        this.payer = payer;
    }

    public Data_Payer getBuyer() {
        return buyer;
    }

    public void setBuyer(Data_Payer buyer) {
        this.buyer = buyer;
    }
}
