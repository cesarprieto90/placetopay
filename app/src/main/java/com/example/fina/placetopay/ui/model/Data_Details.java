package com.example.fina.placetopay.ui.model;

import io.realm.RealmObject;

public class Data_Details extends RealmObject {
    private String kind = "";
    private String amount = "";

    public Data_Details(String kind, String amount) {
        this.kind = kind;
        this.amount = amount;
    }

    public Data_Details() {
    }

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
}
