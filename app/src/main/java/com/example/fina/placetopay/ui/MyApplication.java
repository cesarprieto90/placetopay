package com.example.fina.placetopay.ui;

import android.app.Application;

import com.example.fina.placetopay.ui.di.components.ApplicationComponent;
import com.example.fina.placetopay.ui.di.components.DaggerApplicationComponent;
import com.example.fina.placetopay.ui.di.modules.ApplicationModule;

import io.realm.Realm;
import io.realm.RealmConfiguration;


/**
 * Created by anujgupta on 22/01/18.
 */

public class MyApplication extends Application {

    private static ApplicationComponent applicationComponent;

      public MyApplication(){

      }

    @Override
    public void onCreate() {
        super.onCreate();

        Realm.init(this);

        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder().build();
        Realm.setDefaultConfiguration(realmConfiguration); // M

        applicationComponent = DaggerApplicationComponent
                                .builder()
                                .applicationModule(new ApplicationModule(this))
                                .build();

    }

    public ApplicationComponent getApplicationComponent(){
          return applicationComponent;
      }

}
