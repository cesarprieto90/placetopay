package com.example.fina.placetopay.ui.model;

import io.realm.RealmList;
import io.realm.RealmObject;

public class Data_Search_Response extends RealmObject {

    private Data_Status status = new Data_Status();
    private RealmList<Data_Response_Object> transactions = new RealmList<>();

    public Data_Search_Response(Data_Status dataStatus, RealmList<Data_Response_Object> dataResponseObject) {
        this.status = dataStatus;
        this.transactions = dataResponseObject;
    }

    public Data_Search_Response() {
    }

    public Data_Status getStatus() {
        return status;
    }

    public void setStatus(Data_Status status) {
        this.status = status;
    }

    public RealmList<Data_Response_Object> getTransactions() {
        return transactions;
    }

    public void setTransactions(RealmList<Data_Response_Object> transactions) {
        this.transactions = transactions;
    }

    public Data_Status getDataStatus() {
        return status;
    }

    public void setDataStatus(Data_Status dataStatus) {
        this.status = dataStatus;
    }

    public RealmList<Data_Response_Object> getDataResponseObject() {
        return transactions;
    }

    public void setDataResponseObject(RealmList<Data_Response_Object> dataResponseObject) {
        this.transactions = dataResponseObject;
    }
}
