package com.example.fina.placetopay.ui.model;

public class Data_Instrument {

    private Data_Card card=new Data_Card();

    public Data_Instrument(Data_Card card) {
        this.card = card;
    }

    public Data_Instrument() {
    }

    public Data_Card getCard() {
        return card;
    }

    public void setCard(Data_Card card) {
        this.card = card;
    }
}
