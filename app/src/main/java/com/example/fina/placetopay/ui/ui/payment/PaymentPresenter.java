package com.example.fina.placetopay.ui.ui.payment;

import android.util.Log;

import com.example.fina.placetopay.ui.model.Data_Object;
import com.example.fina.placetopay.ui.model.Data_Response_Object;

import com.example.fina.placetopay.ui.network.NetworkClient;
import com.example.fina.placetopay.ui.network.NetworkInterface;
import com.google.gson.Gson;

import org.json.JSONObject;


import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import io.realm.internal.IOException;
import okhttp3.ResponseBody;
import retrofit2.HttpException;

public class PaymentPresenter implements PaymentPresenterInterface {

    PaymentViewInterface paymentViewInterface;
    private String TAG = "LoginPresenter";
    private Data_Object dataObjecterror = new Data_Object();

    public PaymentPresenter(PaymentViewInterface paymentViewInterface) {
        this.paymentViewInterface = paymentViewInterface;
    }

    @Override
    public void getPayment(Data_Object dataObject) {
        paymentViewInterface.showProgressBar();
        paymentViewInterface.showDatos();
        if (paymentViewInterface.validate()) {
            Gson gson = new Gson();
            String a = gson.toJson(dataObject);
            dataObjecterror = dataObject;
            getObservable(dataObject).subscribeWith(getObserver());
        } else {
            paymentViewInterface.hideProgressBar();
            paymentViewInterface.hideDatos();
        }
    }

    public Observable<Data_Response_Object> getObservable(Data_Object dataObject) {
        return NetworkClient.getRetrofit().create(NetworkInterface.class)
                .getPayment(dataObject).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public DisposableObserver<Data_Response_Object> getObserver() {
        return new DisposableObserver<Data_Response_Object>() {

            @Override
            public void onNext(@NonNull Data_Response_Object dataResponseObject) {
                Log.d(TAG, "OnNext");
                paymentViewInterface.statePayment(dataResponseObject);
                paymentViewInterface.hideProgressBar();
                paymentViewInterface.hideDatos();
                paymentViewInterface.AlertaTransaccion(dataResponseObject);

            }

            @Override
            public void onError(@NonNull Throwable e) {

                String errMsg = "";

                try {
                    if (e instanceof SocketTimeoutException) {
                        paymentViewInterface.statePaymentError(dataObjecterror);
                        errMsg="Timeout";
                    }
                    if(e instanceof UnknownHostException){
                        errMsg="Verifique la red.";
                    } else {
                        ResponseBody response = ((HttpException) e).response().errorBody();
                        JSONObject json = null;
                        json = new JSONObject(new String(response.bytes()));
                        errMsg = json.getString("status");
                    }
                } catch (Exception error) {
                    error.printStackTrace();
                }

                paymentViewInterface.hideProgressBar();
                paymentViewInterface.hideDatos();
                paymentViewInterface.displayError(errMsg);
            }

            @Override
            public void onComplete() {
                Log.d(TAG, "Completed");
                paymentViewInterface.hideProgressBar();
            }
        };
    }
}
