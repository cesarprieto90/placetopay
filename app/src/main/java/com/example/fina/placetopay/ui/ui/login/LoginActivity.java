package com.example.fina.placetopay.ui.ui.login;

import android.content.Intent;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.fina.placetopay.R;
import com.example.fina.placetopay.ui.Utils.Constants;
import com.example.fina.placetopay.ui.Utils.Utils;
import com.example.fina.placetopay.ui.model.Data_Usuario;
import com.example.fina.placetopay.ui.ui.buy.BuyActivity;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;

public class LoginActivity extends AppCompatActivity implements LoginViewInterface {

    private List<Data_Usuario> listPerson = new ArrayList<Data_Usuario>();
    private Realm realm;
    private LoginPresenter loginPresenter;

    private TextInputEditText txtUser, txtPassword;
    private ProgressBar progressBar;

    private String User, Password;

    MaterialButton btnLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        setupView();
        setupMVP();
        login();
    }

    private void login() {

        validarSesion();
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Data_Usuario dataUsuario = new Data_Usuario(txtUser.getText().toString(), txtPassword.getText().toString());
                loginPresenter.getLogin(dataUsuario);
            }
        });
    }

    private void setupMVP() {
        loginPresenter = new LoginPresenter(this);
    }

    private void setupView() {
        realm = Realm.getDefaultInstance();
        txtUser = findViewById(R.id.txtUser);
        txtPassword = findViewById(R.id.txtPassword);
        btnLogin = findViewById(R.id.btnLogin);
        progressBar = findViewById(R.id.progressBar);
    }


    private void validarSesion() {
        RealmQuery<Data_Usuario> query = realm.where(Data_Usuario.class);
        RealmResults<Data_Usuario> employees = query.findAll();
        if (employees.size() > 0) {
            Toast.makeText(LoginActivity.this, "Sesion cargada", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(LoginActivity.this, BuyActivity.class);
            startActivity(intent);
        }
    }

    @Override
    public void showProgressBar() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressBar() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void displayError(String mensaje) {
        Toast.makeText(this, mensaje, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void stateLogin(final Data_Usuario p) {
        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm bgRealm) {
                bgRealm.copyToRealm(p);
            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                Intent intent = new Intent(LoginActivity.this, BuyActivity.class);
                startActivity(intent);
            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                displayError("Error Guardando");
            }
        });
    }

    @Override
    public boolean validate() {
        User = txtUser.getText().toString().trim();
        Password = txtPassword.getText().toString().trim();

        txtUser.setError(null);
        txtPassword.setError(null);
        if (User.isEmpty()) {
            txtUser.setError("Digite el usuario.");
            return false;
        }
        if (Password.isEmpty()) {
            txtPassword.setError("Digite el password.");
            return false;
        }
        return true;
    }

    @Override
    public void onBackPressed() {

    }
}
