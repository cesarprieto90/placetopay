package com.example.fina.placetopay.ui.ui.payment;


import com.example.fina.placetopay.ui.model.Data_Object;
import com.example.fina.placetopay.ui.model.Data_Response_Object;

/**
 * Created by anujgupta on 26/12/17.
 */

public interface PaymentViewInterface {


    void showProgressBar();

    void hideProgressBar();

    void displayError(String s);

    void statePayment(Data_Response_Object dataResponseObject);

    void statePaymentError(Data_Object dataObject);

    void AlertaTransaccion(Data_Response_Object dataResponseObject);

    void showDatos();

    void hideDatos();

    boolean validate();

}
