package com.example.fina.placetopay.ui.model;

import com.example.fina.placetopay.ui.Utils.Constants;

import io.realm.RealmList;
import io.realm.RealmObject;

public class Data_Amount extends RealmObject {
    private RealmList<Data_Taxes> arrayTaxes;
    private RealmList<Data_Details> arrayDetails;
    private String currency= Constants.Currency;
    private String total="";

    public Data_Amount(RealmList<Data_Taxes> arrayTaxes, RealmList<Data_Details> arrayDetails, String currency, String total) {
        this.arrayTaxes = arrayTaxes;
        this.arrayDetails = arrayDetails;
        this.currency = currency;
        this.total = total;
    }

    public Data_Amount() {
    }

    public RealmList<Data_Taxes> getArrayTaxes() {
        return arrayTaxes;
    }

    public void setArrayTaxes(RealmList<Data_Taxes> arrayTaxes) {
        this.arrayTaxes = arrayTaxes;
    }

    public RealmList<Data_Details> getArrayDetails() {
        return arrayDetails;
    }

    public void setArrayDetails(RealmList<Data_Details> arrayDetails) {
        this.arrayDetails = arrayDetails;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }
}
