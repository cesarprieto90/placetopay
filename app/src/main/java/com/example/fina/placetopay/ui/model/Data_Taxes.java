package com.example.fina.placetopay.ui.model;

import io.realm.RealmObject;

public class Data_Taxes extends RealmObject {

    private String kind="";
    private String amount="";
    private String base="";

    public Data_Taxes(String kind, String amount, String base) {
        this.kind = kind;
        this.amount = amount;
        this.base = base;
    }

    public Data_Taxes() {
    }

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getBase() {
        return base;
    }

    public void setBase(String base) {
        this.base = base;
    }
}
