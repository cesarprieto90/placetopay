package com.example.fina.placetopay.ui.ui.buy.ui.Payment;

import com.example.fina.placetopay.ui.model.Data_Object;

/**
 * Created by anujgupta on 26/12/17.
 */

public interface PaymentfragmentPresenterInterface {

    void getPayment(Data_Object dataObject);
}
