package com.example.fina.placetopay.ui.model;

import io.realm.RealmObject;

public class Data_Conversion extends RealmObject {

    private Data_From_To from;
    private Data_From_To to;
    private String factor;

    public Data_Conversion(Data_From_To from, Data_From_To to, String factor) {
        this.from = from;
        this.to = to;
        this.factor = factor;
    }

    public Data_Conversion() {
    }

    public Data_From_To getFrom() {
        return from;
    }

    public void setFrom(Data_From_To from) {
        this.from = from;
    }

    public Data_From_To getTo() {
        return to;
    }

    public void setTo(Data_From_To to) {
        this.to = to;
    }

    public String getFactor() {
        return factor;
    }

    public void setFactor(String factor) {
        this.factor = factor;
    }
}
