package com.example.fina.placetopay.ui.ui.Adapter;

import android.view.View;

/**
 * Created by cesarprieto on 07/11/18.
 */

public interface ItemClickListener {
    void onClick(View view, int position, boolean isLongClick);
}
