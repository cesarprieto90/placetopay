package com.example.fina.placetopay.ui.ui.buy.ui.HisPayment;

import android.util.Log;

import com.example.fina.placetopay.ui.model.Data_Object;
import com.example.fina.placetopay.ui.model.Data_Response_Object;
import com.example.fina.placetopay.ui.model.Data_Search;
import com.example.fina.placetopay.ui.model.Data_Search_Response;
import com.example.fina.placetopay.ui.network.NetworkClient;
import com.example.fina.placetopay.ui.network.NetworkInterface;
import com.example.fina.placetopay.ui.ui.buy.ui.Payment.PaymentFragmentPresenter;
import com.example.fina.placetopay.ui.ui.payment.PaymentPresenterInterface;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.HttpException;

public class HisPaymentPresenter implements HisPaymentPresenterInterface {

    HisPaymentViewInterface hisPaymentViewInterface;
    private String TAG = "LoginPresenter";
    private Data_Object dataObjecterror = new Data_Object();

    public HisPaymentPresenter(HisPaymentViewInterface hisPaymentViewInterface) {
        this.hisPaymentViewInterface = hisPaymentViewInterface;
    }

    @Override
    public void gethisPayment(Data_Search dataSearch) {
        hisPaymentViewInterface.showProgressBar();
        Gson gson = new Gson();
        String a = gson.toJson(dataSearch);
        getObservable(dataSearch).subscribeWith(getObserver());

    }


    public Observable<Data_Search_Response> getObservable(Data_Search dataSearch) {
        return NetworkClient.getRetrofit().create(NetworkInterface.class)
                .getPayment(dataSearch).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public DisposableObserver<Data_Search_Response> getObserver() {
        return new DisposableObserver<Data_Search_Response>() {

            @Override
            public void onNext(@NonNull Data_Search_Response dataResponseObject) {
                Log.d(TAG, "OnNext");

                hisPaymentViewInterface.hideProgressBar();
                hisPaymentViewInterface.stateHisPayment(dataResponseObject);

            }

            @Override
            public void onError(@NonNull Throwable e) {
                String errMsg = "";
                try {
                    if (e instanceof SocketTimeoutException) {
                        errMsg = "Timeout";
                    }
                    if (e instanceof UnknownHostException) {
                        errMsg = "Verifique la red.";
                    } else {
                        ResponseBody response = ((HttpException) e).response().errorBody();
                        JSONObject json = null;
                        json = new JSONObject(new String(response.bytes()));
                        errMsg = json.getString("status");
                    }
                } catch (Exception error) {

                    error.printStackTrace();
                }
                hisPaymentViewInterface.hideProgressBar();
                hisPaymentViewInterface.displayError(errMsg);
            }

            @Override
            public void onComplete() {
                Log.d(TAG, "Completed");
                hisPaymentViewInterface.hideProgressBar();
            }
        };
    }
}
