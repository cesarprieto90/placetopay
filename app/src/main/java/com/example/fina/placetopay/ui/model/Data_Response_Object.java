package com.example.fina.placetopay.ui.model;

import io.realm.RealmObject;

public class Data_Response_Object extends RealmObject {

    private Data_Status status=new Data_Status();
    private String date;
    private String transactionDate;
    private String internalReference;
    private String reference;
    private String paymentMethod;
    private String franchise;
    private String franchiseName;
    private String issuerName;
    private Data_Amount amount=new Data_Amount();
    private Data_Conversion conversion =new Data_Conversion();
    private String authorization;
    private String receipt;
    private String type;
    private String refunded;
    private String lastDigits;
    private String provider;
    private String discount;
    private String estado="";

    private Data_Processor_Fields processorFields=new Data_Processor_Fields();

    public Data_Status getStatus() {
        return status;
    }

    public void setStatus(Data_Status status) {
        this.status = status;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(String transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getInternalReference() {
        return internalReference;
    }

    public void setInternalReference(String internalReference) {
        this.internalReference = internalReference;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getFranchise() {
        return franchise;
    }

    public void setFranchise(String franchise) {
        this.franchise = franchise;
    }

    public String getFranchiseName() {
        return franchiseName;
    }

    public void setFranchiseName(String franchiseName) {
        this.franchiseName = franchiseName;
    }

    public String getIssuerName() {
        return issuerName;
    }

    public void setIssuerName(String issuerName) {
        this.issuerName = issuerName;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Data_Amount getAmount() {
        return amount;
    }

    public void setAmount(Data_Amount amount) {
        this.amount = amount;
    }

    public Data_Conversion getConversion() {
        return conversion;
    }

    public void setConversion(Data_Conversion conversion) {
        this.conversion = conversion;
    }

    public String getAuthorization() {
        return authorization;
    }

    public void setAuthorization(String authorization) {
        this.authorization = authorization;
    }

    public String getReceipt() {
        return receipt;
    }

    public void setReceipt(String receipt) {
        this.receipt = receipt;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getRefunded() {
        return refunded;
    }

    public void setRefunded(String refunded) {
        this.refunded = refunded;
    }

    public String getLastDigits() {
        return lastDigits;
    }

    public void setLastDigits(String lastDigits) {
        this.lastDigits = lastDigits;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public Data_Processor_Fields getProcessorFields() {
        return processorFields;
    }

    public void setProcessorFields(Data_Processor_Fields processorFields) {
        this.processorFields = processorFields;
    }
}
