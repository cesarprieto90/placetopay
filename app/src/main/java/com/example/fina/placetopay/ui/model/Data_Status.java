package com.example.fina.placetopay.ui.model;

import io.realm.RealmObject;

public class Data_Status extends RealmObject {
    private String status="";
    private String reason="";
    private String message="";
    private String date="";

    public Data_Status(String status, String reason, String message, String date) {
        this.status = status;
        this.reason = reason;
        this.message = message;
        this.date = date;
    }

    public Data_Status() {
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
