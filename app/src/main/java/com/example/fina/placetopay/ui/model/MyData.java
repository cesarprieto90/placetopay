package com.example.fina.placetopay.ui.model;

import com.example.fina.placetopay.R;

public class MyData {
    //list of android version names
    public static String[] mombre = {"Balon de futbol", "Automovil de juguete", "Moto de juguete", "Casco de moto",
            "Computador"};
    //list of android version
    public static String[] datoPrecio = {"Precio: 11000", "Precio: 15000", "Precio: 14000",
            "Precio: 13000", "Precio: 20000"};
    //list of android images
    public static Integer[] iconos = {R.drawable.ic_balon, R.drawable.ic_carro, R.drawable.ic_moto,
            R.drawable.ic_casco, R.drawable.ic_pc};
    public static Integer[] id_ = {0, 1, 2, 3, 4};
    public static Integer[] Precio_ = {11000, 15000, 14000, 13000, 20000};
}