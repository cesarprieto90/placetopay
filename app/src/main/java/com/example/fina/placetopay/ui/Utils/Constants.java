package com.example.fina.placetopay.ui.Utils;

import java.math.BigInteger;
import java.security.SecureRandom;

public class Constants {

    /**
     * Constantes del login para la peticion del web service.
     */

    public static String Login="6dd490faf9cb87a9862245da41170ff2";
    public static String TranKe="024h1IlD";
    public static String Nonce =new BigInteger(40, new SecureRandom()).toString();
    public static String Locale ="es_CO";
    public static String Reference ="TEST_";
    public static String Currency ="COP";
    public static String Document ="CC";


}
