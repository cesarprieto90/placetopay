package com.example.fina.placetopay.ui.ui.buy.ui.HisPayment;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.fina.placetopay.R;
import com.example.fina.placetopay.ui.Utils.Constants;
import com.example.fina.placetopay.ui.Utils.Utils;
import com.example.fina.placetopay.ui.model.Data_Auth;
import com.example.fina.placetopay.ui.model.Data_Object;
import com.example.fina.placetopay.ui.model.Data_Producto;
import com.example.fina.placetopay.ui.model.Data_Response_Object;
import com.example.fina.placetopay.ui.model.Data_Search;
import com.example.fina.placetopay.ui.model.Data_Search_Response;
import com.example.fina.placetopay.ui.model.MyData;
import com.example.fina.placetopay.ui.ui.Adapter.ProductoAdapter;
import com.example.fina.placetopay.ui.ui.Adapter.TransaccionAdapter;
import com.example.fina.placetopay.ui.ui.buy.ui.Payment.PaymentFragmentPresenter;
import com.example.fina.placetopay.ui.ui.payment.PaymentPresenter;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;


public class HisPaymentFragment extends Fragment implements HisPaymentViewInterface{

    private Realm realm;
    private ProgressBar progressBar;
    private static RecyclerView recyclerView;
    private static RecyclerView.Adapter adapter;
    private ImageView btnActualizar;
    private View root;
    private HisPaymentPresenter hisPaymentPresenter;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_hispayment, container, false);

        setupView();
        setupMVP();
        hisPayment();

        return root;
    }

    private void hisPayment() {

        RealmResults<Data_Response_Object> dataReponse = realm.where(Data_Response_Object.class).sort("date", Sort.DESCENDING).findAll();

        adapter = new TransaccionAdapter(dataReponse,getContext());
        recyclerView.setAdapter(adapter);

        btnActualizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RealmResults<Data_Response_Object> dataReponse1= realm.where(Data_Response_Object.class).equalTo("estado", "PENDING").findAll();
                List<Data_Response_Object> arrayObject=realm.copyFromRealm(dataReponse1);
                if(arrayObject.size()>0) {
                    for (int i = 0; i < arrayObject.size(); i++) {

                        Data_Search dataSearch = new Data_Search();
                        dataSearch.setAmount(arrayObject.get(i).getAmount());
                        dataSearch.setAuth(dataAuth());
                        dataSearch.setReference(arrayObject.get(i).getReference());

                        hisPaymentPresenter.gethisPayment(dataSearch);

                    }
                } else
                    displayError("Datos Actualizados.");

                String c="";
            }
        });
    }

    private void setupMVP() {
        hisPaymentPresenter = new HisPaymentPresenter(this);
    }

    private void setupView() {
        realm = Realm.getDefaultInstance();
        recyclerView = root.findViewById(R.id.rvListaTransacciones);
        btnActualizar = root.findViewById(R.id.btnActualizar);
        progressBar = root.findViewById(R.id.progressBar);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

    }

    private Data_Auth dataAuth(){
        String Login = Constants.Login;
        String nTrankey = "";
        String fechaIso = Utils.fechaISO();
        String nNonce = "";
        try {
            nTrankey = Utils.base64(Utils.SHA256(Constants.Nonce + fechaIso + Constants.TranKe));
            nNonce = Utils.base64(Constants.Nonce.getBytes());
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        Data_Auth dataAuth = new Data_Auth(Login, nTrankey, nNonce, fechaIso);
        return dataAuth;
    }

    @Override
    public void showProgressBar() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressBar() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void displayError(String s) {
        Toast.makeText(getContext(), s, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void stateHisPayment(Data_Search_Response dataSearchResponse) {

        if(dataSearchResponse.getTransactions().get(0).getStatus().getStatus().toString().equals("APPROVED")){

            realm.beginTransaction();
            RealmResults rows= realm.where(Data_Response_Object.class).
                    equalTo("reference", dataSearchResponse.getDataResponseObject().get(0).getReference()).findAll();;
            rows.deleteAllFromRealm();
            realm.commitTransaction();

            realm.beginTransaction();
            realm.copyToRealm(dataSearchResponse.getTransactions().get(0));
            realm.commitTransaction();

            RealmResults<Data_Response_Object> dataReponse = realm.where(Data_Response_Object.class).sort("date", Sort.DESCENDING).findAll();

            adapter = new TransaccionAdapter(dataReponse,getContext());
            recyclerView.setAdapter(adapter);

        }
            displayError("Datos actualizados");

    }

}