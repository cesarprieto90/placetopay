package com.example.fina.placetopay.ui.model;

import com.example.fina.placetopay.ui.Utils.Constants;

import io.realm.RealmObject;

public class Data_Payment extends RealmObject {

    private String reference= Constants.Reference;
    private String description="";
    private Data_Amount amount=new Data_Amount();

    public Data_Payment(String reference, String description, Data_Amount amount) {
        this.reference = reference;
        this.description = description;
        this.amount = amount;
    }

    public Data_Payment() {
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getDescription() {
        return description;
    }


    public Data_Amount getAmount() {
        return amount;
    }

    public void setAmount(Data_Amount amount) {
        this.amount = amount;
    }

    public void setDescription(String description) {
        this.description = description;
    }


}
