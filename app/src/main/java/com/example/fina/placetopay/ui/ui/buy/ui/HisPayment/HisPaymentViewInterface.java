package com.example.fina.placetopay.ui.ui.buy.ui.HisPayment;


import com.example.fina.placetopay.ui.model.Data_Object;
import com.example.fina.placetopay.ui.model.Data_Response_Object;
import com.example.fina.placetopay.ui.model.Data_Search_Response;

/**
 * Created by anujgupta on 26/12/17.
 */

public interface HisPaymentViewInterface {

    void showProgressBar();

    void hideProgressBar();

    void displayError(String s);

    void stateHisPayment(Data_Search_Response dataSearchResponse);


}
