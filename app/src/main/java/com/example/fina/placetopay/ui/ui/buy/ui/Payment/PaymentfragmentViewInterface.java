package com.example.fina.placetopay.ui.ui.buy.ui.Payment;


import com.example.fina.placetopay.ui.model.Data_Object;
import com.example.fina.placetopay.ui.model.Data_Response_Object;

/**
 * Created by anujgupta on 26/12/17.
 */

public interface PaymentfragmentViewInterface {

    void statePaymentError(Data_Object dataObject);

    void showDatos();

    void hideDatos();

    void showProgressBar();

    void hideProgressBar();

    void displayError(String s);

    void statePayment(Data_Response_Object dataResponseObject);

    void AlertaTransaccion(Data_Response_Object dataResponseObject);

    boolean validate();

}
