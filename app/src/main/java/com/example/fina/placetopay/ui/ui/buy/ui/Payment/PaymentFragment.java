package com.example.fina.placetopay.ui.ui.buy.ui.Payment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.fina.placetopay.R;
import com.example.fina.placetopay.ui.Utils.Constants;
import com.example.fina.placetopay.ui.Utils.Utils;
import com.example.fina.placetopay.ui.model.Data_Amount;
import com.example.fina.placetopay.ui.model.Data_Auth;
import com.example.fina.placetopay.ui.model.Data_Card;
import com.example.fina.placetopay.ui.model.Data_Object;
import com.example.fina.placetopay.ui.model.Data_Payer;
import com.example.fina.placetopay.ui.model.Data_Payment;
import com.example.fina.placetopay.ui.model.Data_Response_Object;
import com.example.fina.placetopay.ui.model.Data_Status;
import com.example.fina.placetopay.ui.ui.buy.BuyActivity;
import com.example.fina.placetopay.ui.ui.payment.PaymentActivity;
import com.example.fina.placetopay.ui.ui.payment.PaymentPresenter;
import com.example.fina.placetopay.ui.ui.payment.PaymentViewInterface;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;

import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import io.realm.Realm;


public class PaymentFragment extends Fragment implements PaymentfragmentViewInterface {

    private PaymentFragmentPresenter paymentPresenter;
    private TextInputEditText txtPrecio, txtNombre, txtCedula, txtEmail, txtCelular, txtNumeroTarjeta, txtMes, txtYear, txtCodigoSeguridad;
    private MaterialButton btnPagar;
    private ProgressBar progressBar;
    private String Precio, Nombre, Cedula, Email, Celular, NumeroTarjeta, Mes, Year, CodigoSeguridad;
    private Realm realm;
    private LinearLayout lyDatos;

    View root;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        root = inflater.inflate(R.layout.fragment_payment, container, false);

        setupView();
        setupMVP();
        payment();

        return root;
    }

    private void payment() {
        paymentPresenter = new PaymentFragmentPresenter(this);
    }

    private void setupMVP() {

        btnPagar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                paymentPresenter.getPayment(dataObject());
            }
        });
    }

    private void setupView() {
        Toolbar myToolbar = root.findViewById(R.id.toolbar);

        realm = Realm.getDefaultInstance();
        txtPrecio = root.findViewById(R.id.txtValor);
        txtNombre = root.findViewById(R.id.txtNombre);
        txtCedula = root.findViewById(R.id.txtCedula);
        txtEmail = root.findViewById(R.id.txtEmail);
        txtCelular = root.findViewById(R.id.txtCelular);
        txtNumeroTarjeta = root.findViewById(R.id.txtNumeroTarjeta);
        txtMes = root.findViewById(R.id.txtMes);
        txtYear = root.findViewById(R.id.txtYear);
        lyDatos = root.findViewById(R.id.lyDatos);
        txtCodigoSeguridad = root.findViewById(R.id.txtCodigoSeguridad);

        btnPagar = root.findViewById(R.id.btnPagar);
        progressBar = root.findViewById(R.id.progressBar);

    }

    private Data_Object dataObject() {

        String Login = Constants.Login;
        String nTrankey = "";
        String fechaIso = Utils.fechaISO();
        String nNonce = "";
        try {
            nTrankey = Utils.base64(Utils.SHA256(Constants.Nonce + fechaIso + Constants.TranKe));
            nNonce = Utils.base64(Constants.Nonce.getBytes());
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        Data_Object data_object = new Data_Object();
        Data_Auth dataAuth = new Data_Auth(Login, nTrankey, nNonce, fechaIso);
        Data_Amount dataAmount = new Data_Amount(null, null, Constants.Currency, txtPrecio.getText().toString());
        Data_Payment dataPayment = new Data_Payment(Constants.Reference+new BigInteger(20, new SecureRandom()).toString(), "", dataAmount);
        Data_Card dataCard = new Data_Card(txtNumeroTarjeta.getText().toString(), txtMes.getText().toString(), txtYear.getText().toString(),
                txtCodigoSeguridad.getText().toString());
        Data_Payer dataPayer = new Data_Payer(txtCedula.getText().toString(), Constants.Document, txtNombre.getText().toString(),
                txtNombre.getText().toString(), txtEmail.getText().toString(), txtEmail.getText().toString());

        data_object.setAuth(dataAuth);
        data_object.setPayment(dataPayment);
        data_object.getInstrument().setCard(dataCard);
        data_object.setPayer(dataPayer);
        data_object.setBuyer(dataPayer);
        return data_object;
    }


    @Override
    public void statePaymentError(Data_Object dataObject) {
        Data_Status dataStatus = new Data_Status("PENDING","00","Pendiente",Utils.fechaISO());
        Data_Response_Object dataResponseObject = new Data_Response_Object();
        dataResponseObject.setAmount(dataObject.getPayment().getAmount());
        dataResponseObject.setStatus(dataStatus);
        dataResponseObject.setEstado("PENDING");
        dataResponseObject.setReference(dataObject.getPayment().getReference());

        statePayment(dataResponseObject);
        AlertaTransaccion(dataResponseObject);
    }

    @Override
    public void showDatos() {
        lyDatos.setVisibility(View.GONE);
    }

    @Override
    public void hideDatos() {
        lyDatos.setVisibility(View.VISIBLE);
    }

    @Override
    public void showProgressBar() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressBar() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void displayError(String mensaje) {
        Toast.makeText(getContext(), mensaje, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void statePayment(Data_Response_Object dataResponseObject) {

        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm bgRealm) {
                bgRealm.copyToRealm(dataResponseObject);
            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                displayError("Transaccion Realizada.");
            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                displayError("Error Guardando");
            }
        });

    }

    @Override
    public void AlertaTransaccion(Data_Response_Object dataResponseObject) {
        AlertDialog.Builder dialogo1 = new AlertDialog.Builder(getContext());
        dialogo1.setTitle("Transacción");
        dialogo1.setMessage("Fecha: " + dataResponseObject.getStatus().getDate() +
                "\nEstado: " + dataResponseObject.getStatus().getStatus() +
                "\nMensaje: " + dataResponseObject.getStatus().getMessage() +
                "\nReferencia Interna: " + dataResponseObject.getInternalReference() +
                "\nTotal: " + dataResponseObject.getAmount().getTotal());

        dialogo1.setCancelable(false);
        dialogo1.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogo1, int id) {
                Intent intent = new Intent(getContext(), BuyActivity.class);
                startActivity(intent);
            }
        });

        dialogo1.show();
    }

    @Override
    public boolean validate() {
        Precio = txtPrecio.getText().toString().trim();
        Nombre = txtNombre.getText().toString().trim();
        Cedula = txtCedula.getText().toString().trim();
        Email = txtEmail.getText().toString().trim();
        Celular = txtCelular.getText().toString().trim();
        NumeroTarjeta = txtNumeroTarjeta.getText().toString().trim();
        Mes = txtMes.getText().toString().trim();
        Year = txtYear.getText().toString().trim();
        CodigoSeguridad = txtCodigoSeguridad.getText().toString().trim();

        txtPrecio.setError(null);
        txtNombre.setError(null);
        txtCedula.setError(null);
        txtEmail.setError(null);
        txtCelular.setError(null);
        txtNumeroTarjeta.setError(null);
        txtMes.setError(null);
        txtYear.setError(null);
        txtCodigoSeguridad.setError(null);

        if (Nombre.isEmpty()) {
            txtNombre.setError("Digite el valor.");
            return false;
        }
        if (Cedula.isEmpty()) {
            txtCedula.setError("Digite el valor.");
            return false;
        }
        if (Email.isEmpty()) {
            txtEmail.setError("Digite el valor.");
            return false;
        }
        if (Celular.isEmpty()) {
            txtCelular.setError("Digite el valor.");
            return false;
        }
        if (NumeroTarjeta.isEmpty()) {
            txtNumeroTarjeta.setError("Digite el valor.");
            return false;
        }
        if (Mes.isEmpty()) {
            txtMes.setError("Digite el valor.");
            return false;
        }
        if (Year.isEmpty()) {
            txtYear.setError("Digite el valor.");
            return false;
        }
        if (NumeroTarjeta.isEmpty()) {
            txtNumeroTarjeta.setError("Digite el valor.");
            return false;
        }
        if (CodigoSeguridad.isEmpty()) {
            txtCodigoSeguridad.setError("Digite el valor.");
            return false;
        }
        if (Precio.isEmpty()) {
            txtPrecio.setError("Digite el valor.");
            return false;
        }

        return true;
    }
}