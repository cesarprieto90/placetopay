package com.example.fina.placetopay.ui.model;

import io.realm.RealmObject;

public class Data_From_To extends RealmObject {

    private String currency="";
    private String total="";

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }
}
