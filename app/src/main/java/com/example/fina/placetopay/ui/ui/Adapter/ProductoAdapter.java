package com.example.fina.placetopay.ui.ui.Adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Build;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.example.fina.placetopay.R;
import com.example.fina.placetopay.ui.model.Data_Producto;
import com.example.fina.placetopay.ui.ui.payment.PaymentActivity;

import java.util.ArrayList;

public class ProductoAdapter extends RecyclerView.Adapter<ProductoAdapter.OrdenHolder> {

    private ArrayList<Data_Producto> dataProductos;
    private Context context;

    public ProductoAdapter(ArrayList<Data_Producto> dataProductos, Context context) {
        this.dataProductos = dataProductos;
        this.context = context;
    }

    @Override
    public OrdenHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.item_pruducto,parent,false);
        OrdenHolder mh = new OrdenHolder(v);

        return mh;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onBindViewHolder(OrdenHolder holder, int position) {
        try {
            holder.textViewName.setText(dataProductos.get(position).getName());
            holder.textViewVersion.setText(dataProductos.get(position).getVersion());
            holder.imageView.setImageResource(dataProductos.get(position).getImage());
            holder.setItemClickListener(new ItemClickListener() {
                @Override
                public void onClick(View view, int position, boolean isLongClick) {
                    Intent intent = new Intent(context, PaymentActivity.class);
                    intent.putExtra("valor", dataProductos.get(position).getPrecio());
                    context.startActivity(intent);
                }
            });

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return dataProductos.size();
    }

    public class OrdenHolder extends RecyclerView.ViewHolder  implements View.OnClickListener{


        private ImageView imageView;

        private TextView textViewName,textViewVersion,lblProyecto;

        private ItemClickListener itemClickListener;

        public OrdenHolder(View v) {
            super(v);
            textViewName=v.findViewById(R.id.textViewName);
            textViewVersion = v.findViewById(R.id.textViewVersion);
            imageView=v.findViewById(R.id.imageView);
            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            itemClickListener.onClick(view,getAdapterPosition(),false);
        }

        public void setItemClickListener(ItemClickListener itemClickListener){
            this.itemClickListener = itemClickListener;
        }
    }
}
