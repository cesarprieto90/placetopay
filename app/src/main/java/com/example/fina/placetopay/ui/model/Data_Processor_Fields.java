package com.example.fina.placetopay.ui.model;

import io.realm.RealmObject;

public class Data_Processor_Fields extends RealmObject {

    private String id="";
    private String b24="";

    public Data_Processor_Fields(String id, String b24) {
        this.id = id;
        this.b24 = b24;
    }

    public Data_Processor_Fields() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getB24() {
        return b24;
    }

    public void setB24(String b24) {
        this.b24 = b24;
    }
}
