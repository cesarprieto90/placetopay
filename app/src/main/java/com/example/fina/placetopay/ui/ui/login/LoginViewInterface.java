package com.example.fina.placetopay.ui.ui.login;


import com.example.fina.placetopay.ui.model.Data_Usuario;

/**
 * Created by anujgupta on 26/12/17.
 */

public interface LoginViewInterface {

    void showProgressBar();

    void hideProgressBar();

    void displayError(String s);

    void stateLogin(Data_Usuario Data_Usuario);

    boolean validate();


}
