package com.example.fina.placetopay.ui.ui.buy.ui.home;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.fina.placetopay.R;
import com.example.fina.placetopay.ui.model.Data_Producto;
import com.example.fina.placetopay.ui.model.MyData;
import com.example.fina.placetopay.ui.ui.Adapter.ProductoAdapter;

import java.util.ArrayList;


public class HomeFragment extends Fragment {

    private static RecyclerView.Adapter adapter;
    private static RecyclerView recyclerView;
    private static ArrayList<Data_Producto> data;
    private View root;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        root = inflater.inflate(R.layout.fragment_home, container, false);

        setupView();
        setupMVP();
        login();

        return root;
    }
    private void login() {
        data = new ArrayList<Data_Producto>();
        for (int i = 0; i < MyData.mombre.length; i++) {
            data.add(new Data_Producto(
                    MyData.mombre[i],
                    MyData.datoPrecio[i],
                    MyData.Precio_[i],
                    MyData.id_[i],
                    MyData.iconos[i]
            ));
        }
        adapter = new ProductoAdapter(data,getContext());
        recyclerView.setAdapter(adapter);
    }

    private void setupMVP() {


    }

    private void setupView() {
        recyclerView = root.findViewById(R.id.rvListaArticulos);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
    }


}