package com.example.fina.placetopay.ui.ui.login;

import com.example.fina.placetopay.ui.model.Data_Usuario;

public class LoginPresenter implements LoginPresenterInterface {

    LoginViewInterface loginViewInterface;
    private String TAG = "LoginPresenter";


    public LoginPresenter(LoginViewInterface loginViewInterface) {
        this.loginViewInterface = loginViewInterface;
    }


    @Override
    public void getLogin(Data_Usuario Data_Usuario) {
        loginViewInterface.showProgressBar();
        if (loginViewInterface.validate()) {
            if (Data_Usuario.getUsuario().equals("Pruebas") && Data_Usuario.getPassword().equals("pruebas123")) {
                loginViewInterface.stateLogin(Data_Usuario);
                loginViewInterface.hideProgressBar();
            } else {
                loginViewInterface.hideProgressBar();
                loginViewInterface.displayError("Data_Usuario invalido");
            }
        } else
            loginViewInterface.hideProgressBar();
    }


}
