package com.example.fina.placetopay.ui.di.components;



import com.example.fina.placetopay.ui.di.modules.ApplicationModule;

import dagger.Component;

@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

}
