package com.example.fina.placetopay.ui.model;

import io.realm.RealmObject;

public class Data_Additional extends RealmObject {
    private String SOME_ADDITIONAL="http://example.com/yourcheckout";
    private String merchantCode="";
    private String terminalNumber="";
    private String bin="";
    private String expiration="";
    private String installments="";


    public Data_Additional(String SOME_ADDITIONAL) {
        this.SOME_ADDITIONAL = SOME_ADDITIONAL;
    }

    public Data_Additional() {
    }

    public String getSOME_ADDITIONAL() {
        return SOME_ADDITIONAL;
    }

    public void setSOME_ADDITIONAL(String SOME_ADDITIONAL) {
        this.SOME_ADDITIONAL = SOME_ADDITIONAL;
    }

    public String getMerchantCode() {
        return merchantCode;
    }

    public void setMerchantCode(String merchantCode) {
        this.merchantCode = merchantCode;
    }

    public String getTerminalNumber() {
        return terminalNumber;
    }

    public void setTerminalNumber(String terminalNumber) {
        this.terminalNumber = terminalNumber;
    }

    public String getBin() {
        return bin;
    }

    public void setBin(String bin) {
        this.bin = bin;
    }

    public String getExpiration() {
        return expiration;
    }

    public void setExpiration(String expiration) {
        this.expiration = expiration;
    }

    public String getInstallments() {
        return installments;
    }

    public void setInstallments(String installments) {
        this.installments = installments;
    }
}
