package com.example.fina.placetopay.ui.model;

public class Data_Buyer {
    
    private String document="";
    private String documentType="";
    private String name="";
    private String surname="";
    private String email="";
    private String mobile="";

    public Data_Buyer(String document, String documentType, String name, String surname, String email, String mobile) {
        this.document = document;
        this.documentType = documentType;
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.mobile = mobile;
    }

    public Data_Buyer() {
    }

    public String getDocument() {
        return document;
    }

    public void setDocument(String document) {
        this.document = document;
    }

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }
}
